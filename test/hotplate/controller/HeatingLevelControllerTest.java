/**
 * @author Jürgen Göbel, 822602
 * 
 * This source code is part of the Hot Plate Project, written for the module
 * Test Oriented Development at Computer Science Department of Hochschule Fulda.
 */

package hotplate.controller;

import mockit.Deencapsulation;
import org.junit.Test;
import static org.junit.Assert.*;

public class HeatingLevelControllerTest {

    @Test
    public void testIncreaseLevelWithHigherBoundary() {
        HeatingLevelController instance = new HeatingLevelController();
        
        for (int i = 1; i <= 9; i++) {
            instance.increaseLevel();
        }
        instance.increaseLevel();
        assertEquals("Current level is false.", 9, instance.getCurrentLevel());
    }

    @Test
    public void testIncreaseLevelWithCorrectLevelSteps() {
        HeatingLevelController instance = new HeatingLevelController();

        instance.increaseLevel();
        assertEquals("Current level is false.", 2, instance.getCurrentLevel());
    }

    @Test
    public void testDecreaseLevelWithLowerBoundary() {
        HeatingLevelController instance = new HeatingLevelController();

        instance.decreaseLevel();
        assertEquals("Current level is false.", 1, instance.getCurrentLevel());
    }

    @Test
    public void testDecreaseLevelWithCorrectLevelSteps() {
        HeatingLevelController instance = new HeatingLevelController();

        int currentLevel = 5;
        Deencapsulation.setField(instance, currentLevel);

        instance.decreaseLevel();
        assertEquals("Current level is false.", 4, instance.getCurrentLevel());
    }
}
