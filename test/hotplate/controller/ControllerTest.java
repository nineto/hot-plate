/**
 * @author Jürgen Göbel, 822602
 * 
 * This source code is part of the Hot Plate Project, written for the module
 * Test Oriented Development at Computer Science Department of Hochschule Fulda.
 */

package hotplate.controller;

import hotplate.controller.buttons.Button;
import hotplate.controller.buttons.ToggleSwitchButton;
import hotplate.display.Display;
import hotplate.sensor.TempController;
import mockit.Deencapsulation;
import mockit.integration.junit4.JMockit;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class ControllerTest {
    
    public ControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testCurrentStateAfterStartIsStandby() {
        System.out.println("doStart");
        Controller instance = new Controller(
                                 new TempController("Sensor in Fahrenheit"),
                                 new Display("LCD-Display"));
        instance.start();
        
        PlateState currentState;
        currentState = Deencapsulation.getField(instance, "currentState");
        
        assertEquals("Wrong State.", PlateState.STANDBY , currentState);
         
        instance.exit();
    }
    
    @Test
    public void testTransitionFromStandbyToSelection() {
        System.out.println("doStart");
        Controller instance = new Controller(
                                 new TempController("Sensor in Fahrenheit"),
                                 new Display("LCD-Display"));
        
        PlateState currentState;
        
        ToggleSwitchButton powerButton = new ToggleSwitchButton("Power Button");
        powerButton.press();
        Deencapsulation.setField(instance, "powerButton", powerButton);
        
        instance.start();
        
        currentState = Deencapsulation.getField(instance, "currentState");
        assertEquals("Wrong State.", PlateState.SELECTION , currentState);
        
        instance.exit();
    }
    
    @Test
    public void testTransitionFromSelectionToHeating() {
        System.out.println("doStart");
        Controller instance = new Controller(
                                 new TempController("Sensor in Fahrenheit"),
                                 new Display("LCD-Display"));
        
        PlateState currentState;
        currentState = PlateState.SELECTION;
        Deencapsulation.setField(instance, currentState);
        
        Button startHeatingButton = new Button("Start-Heating Button");
        startHeatingButton.press();
        Deencapsulation.setField(instance, "startHeatingButton",
                                            startHeatingButton);
        
        instance.start();
        
        currentState = Deencapsulation.getField(instance, "currentState");
        assertEquals("Wrong State.", PlateState.HEATING, currentState);
        
        instance.exit();
    }
    
    @Test
    public void testTransitionFromHeatingToStandby() {
        System.out.println("doStart");
        Controller instance = new Controller(
                                 new TempController("Sensor in Fahrenheit"),
                                 new Display("LCD-Display"));
        
        PlateState currentState;
        currentState = PlateState.HEATING;
        Deencapsulation.setField(instance, currentState);
        
        ToggleSwitchButton powerButton = new ToggleSwitchButton("Power Button");
        powerButton.press();
        Deencapsulation.setField(instance, "powerButton", powerButton);
        
        instance.start();
        
        currentState = Deencapsulation.getField(instance, "currentState");
        assertEquals("Wrong State.", PlateState.STANDBY, currentState);
        
        instance.exit();
    }

    @Test
    public void testTransitionFromSelectionToStandby() {
        System.out.println("doStart");
        Controller instance = new Controller(
                                 new TempController("Sensor in Fahrenheit"),
                                 new Display("LCD-Display"));
        
        PlateState currentState;
        currentState = PlateState.SELECTION;
        Deencapsulation.setField(instance, currentState);
        
        ToggleSwitchButton powerButton = new ToggleSwitchButton("Power Button");
        powerButton.press();
        Deencapsulation.setField(instance, "powerButton", powerButton);
        
        instance.start();
        
        currentState = Deencapsulation.getField(instance, "currentState");
        assertEquals("Wrong State.", PlateState.STANDBY, currentState);
        
        instance.exit();
    }
    
    @Test
    public void testReflectiveTransitionInSelectionWithIncreaseAndDecrease() {
        System.out.println("doStart");
        Controller controller = new Controller(
                                 new TempController("Sensor in Fahrenheit"),
                                 new Display("LCD-Display"));
        
        PlateState currentState;
        currentState = PlateState.SELECTION;
        Deencapsulation.setField(controller, currentState);
        
        Button increaseButton = new Button("Increase Button");
        Button decreaseButton = new Button("Decrease Button");
        Deencapsulation.setField(controller, "increaseButton", increaseButton);
        Deencapsulation.setField(controller, "decreaseButton", decreaseButton);
        
        controller.start();
        
        increaseButton.press();
            
        currentState = Deencapsulation.getField(controller, "currentState");
        assertEquals("Wrong State.", PlateState.SELECTION, currentState);
        
        increaseButton.reset();     
        decreaseButton.press();
        
        controller.start();
        currentState = Deencapsulation.getField(controller, "currentState");
        assertEquals("Wrong State.", PlateState.SELECTION, currentState);
        
        controller.exit();
    }
    
        @Test
    public void testReflectiveTransitionInHeatingWithIncreaseAndDecrease() {
        System.out.println("doStart");
        Controller controller = new Controller(
                                 new TempController("Sensor in Fahrenheit"),
                                 new Display("LCD-Display"));
        
        PlateState currentState;
        currentState = PlateState.HEATING;
        Deencapsulation.setField(controller, currentState);
        
        Button increaseButton = new Button("Increase Button");
        Button decreaseButton = new Button("Decrease Button");
        Deencapsulation.setField(controller, "increaseButton", increaseButton);
        Deencapsulation.setField(controller, "decreaseButton", decreaseButton);
        
        controller.start();
        
        increaseButton.press();
            
        currentState = Deencapsulation.getField(controller, "currentState");
        assertEquals("Wrong State.", PlateState.HEATING, currentState);
        
        increaseButton.reset();     
        decreaseButton.press();
        
        controller.start();
        currentState = Deencapsulation.getField(controller, "currentState");
        assertEquals("Wrong State.", PlateState.HEATING, currentState);
        
        controller.exit();
    }
}
