/**
 * @author Jürgen Göbel, 822602
 * 
 * This source code is part of the Hot Plate Project, written for the module
 * Test Oriented Development at Computer Science Department of Hochschule Fulda.
 */

import hotplate.controller.Controller;
import hotplate.display.Display;
import hotplate.sensor.TempController;

public class HotPlate {

    public static void main(String[] args) {
        Display lcd = new Display("Level Display");
        TempController tempSens = new TempController(
                "Temperature controller and sensor in Fahrenheit");
        
        Controller ctr = new Controller(tempSens, lcd);
        
        while(!ctr.isExit()) {
            ctr.start();
        }
    }
}
