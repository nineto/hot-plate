/**
 * @author Jürgen Göbel, 822602
 * 
 * This source code is part of the Hot Plate Project, written for the module
 * Test Oriented Development at Computer Science Department of Hochschule Fulda.
 */

package hotplate.controller;

public class HeatingLevelController {
    
    // Constraints for Heating Level
    private static final int MIN_LVL = 1;
    private static final int MAX_LVL = 9;
    private static final int STEPS_LVL = 1;
    
    private int currentLevel = MIN_LVL;

    
     public void increaseLevel() {
        if (currentLevel < MAX_LVL) {
            currentLevel += STEPS_LVL;
        }
    }
    
    public void decreaseLevel() {
        if (currentLevel > MIN_LVL) {
            currentLevel -= STEPS_LVL;
        }
    }

    public int getCurrentLevel() {
        return currentLevel;
    }
}
