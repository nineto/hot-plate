/**
 * @author Jürgen Göbel, 822602
 * 
 * This source code is part of the Hot Plate Project, written for the module
 * Test Oriented Development at Computer Science Department of Hochschule Fulda.
 */

package hotplate.controller.buttons;

public abstract class AbstractButton {
    
    protected String name;
    protected boolean isPressed;
    
    public AbstractButton(String name) {
        this.name = name;
        isPressed = false;
    }
    
    public void press() {
        isPressed = true;
    }
    
    public void reset() {
        isPressed = false;
    }
    
    public boolean isPressed() {
        return isPressed;
    }
}
