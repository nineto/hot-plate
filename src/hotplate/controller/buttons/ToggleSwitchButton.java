/**
 * @author Jürgen Göbel, 822602
 * 
 * This source code is part of the Hot Plate Project, written for the module
 * Test Oriented Development at Computer Science Department of Hochschule Fulda.
 */

package hotplate.controller.buttons;

public class ToggleSwitchButton extends AbstractButton {

    public ToggleSwitchButton(String name) {
        super(name);
    }

    @Override
    public void press() {
        isPressed = !isPressed;
    }
}
