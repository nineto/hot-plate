/**
 * @author Jürgen Göbel, 822602
 * 
 * This source code is part of the Hot Plate Project, written for the module
 * Test Oriented Development at Computer Science Department of Hochschule Fulda.
 */

package hotplate.controller;

import hotplate.controller.buttons.Button;
import hotplate.controller.buttons.ToggleSwitchButton;
import hotplate.display.Display;
import hotplate.sensor.TempController;

public class Controller {

//    // Constraints for Temperature, measured in Fahrenheit
//    private static final int MIN_F = 100;
//    private static final int MAX_F = 500;
//    private static final int STEPS_F = 50;
//    
    private final TempController tempSensor;
    private final Display display;
    private final HeatingLevelController heatingLevelController;
    private boolean isExit;
    private PlateState currentState;

    // Buttons
    private final ToggleSwitchButton powerButton;
    private final Button increaseButton;
    private final Button decreaseButton;
    private final Button startHeatingButton;

    
    public Controller(TempController tempSensor, Display display) { 
        this.tempSensor = tempSensor;
        this.display = display;
        isExit = false;
        currentState = PlateState.STANDBY;
        heatingLevelController = new HeatingLevelController();
        powerButton = new ToggleSwitchButton("Power Button");
        increaseButton = new Button("Increase Button");
        decreaseButton = new Button("Decrease Button");
        startHeatingButton = new Button("Start-Heating Button");
    }    
    
    public void start() {
            switch (currentState) {
               case STANDBY:
                    if(powerButton.isPressed()) {
                       currentState = PlateState.SELECTION;
                       powerButton.reset();
                    }
               break;
                
               case SELECTION:
                   if(powerButton.isPressed()) {
                       currentState = PlateState.STANDBY;
                       powerButton.reset();
                   } else if(startHeatingButton.isPressed()) {
                       currentState = PlateState.HEATING;
                       startHeatingButton.reset();
                   } else if(increaseButton.isPressed()) {
                       heatingLevelController.increaseLevel();
                       increaseButton.reset();
                   } else if(decreaseButton.isPressed()) {
                       heatingLevelController.decreaseLevel();
                       decreaseButton.reset();
                   }
               break;
                
               case HEATING:
                   if(powerButton.isPressed()) {
                       currentState = PlateState.STANDBY;
                       powerButton.reset();
                   } else if(increaseButton.isPressed()) {
                       heatingLevelController.increaseLevel();
                       increaseButton.reset();
                   } else if(decreaseButton.isPressed()) {
                       heatingLevelController.decreaseLevel();
                       decreaseButton.reset();
                   }
               break;
                
               default:
                   System.out.println("Error! Selected state is undefined.");
               break;
            }
        
    }
        
//    public int levelToTemp(int lvl) {
//        return STEPS_F * lvl + STEPS_F;
//    }

    public boolean isExit() {
        return isExit;
    }
    
    public void exit() {
        isExit = true;
    }
}
