/**
 * @author Jürgen Göbel, 822602
 * 
 * This source code is part of the Hot Plate Project, written for the module
 * Test Oriented Development at Computer Science Department of Hochschule Fulda.
 */

package hotplate.display;

public class Display {
    String name;
    
    public Display(String name) {
        name = this.name;
    }
    
    public void sendMsg(float heatLevel) {
        System.out.println("Current Heat Level: " + heatLevel);
    }
}
