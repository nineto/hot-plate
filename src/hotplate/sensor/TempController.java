/**
 * @author Jürgen Göbel, 822602
 * 
 * This source code is part of the Hot Plate Project, written for the module
 * Test Oriented Development at Computer Science Department of Hochschule Fulda.
 */

package hotplate.sensor;

public class TempController {
    int currTemp;
    String name;
    
    public TempController(String name) {
        name = this.name;
    }
    
    private void update() {
        
    }
    
    public String getName() {
        return name;
    }
    
    public int getTemp() {
        return currTemp;
    }
    
    public float toFahrenheit(float celsius) {
        return (float) (celsius * 1.8 + 32);
    }
    
    public float toCelsius(float fahrenheit) {
        return (float) ((fahrenheit - 32) / 1.8);
    }
}
